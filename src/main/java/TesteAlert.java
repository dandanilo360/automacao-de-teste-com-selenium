import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class TesteAlert   {

	
	private WebDriver driver;
	private DSL dsl;
	
	
	@Before 
	public void inicializar () {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		dsl = new DSL(driver);  
  	
}
	
//	@After
//	public void finalizar() {
//		driver.quit();
//	}
	
	@Test
	public void deveInteragirComAlertSimples() { 	
		dsl.clicarBotao("alert");
		String texto = dsl.alertaObterTextoEAceita();
		Assert.assertEquals("Alert Simples", texto);
		
		dsl.escrever("elementosForm:nome",texto);		
	}
	
	@SuppressWarnings("unused")
	@Test
	public void deveInteragirComAlertConfirm() { 
		
		dsl.clicarBotao("confirm");
		Alert alerta = driver.switchTo().alert();
		Assert.assertEquals("Confirm Simples", dsl.alertaObterTextoEAceita());
		Assert.assertEquals("Confirmado", dsl.alertaObterTextoEAceita());
		
		dsl.clicarBotao("confirm");
		Assert.assertEquals("Confirm Simples", dsl.alertaObterTextoENega());
		Assert.assertEquals("Negado", dsl.alertaObterTextoENega());		
	}
	
	@Test
	public void deveInteragirComAlertPrompt() { 	
		dsl.clicarBotao("prompt");
	    Assert.assertEquals("Digite um numero", dsl.alertaObterTexto());
	    dsl.alertaEscrever("3017");
	    assertEquals("Era 3017?", dsl.alertaObterTextoEAceita());
	    Assert.assertEquals(":D", dsl.alertaObterTextoEAceita());	    
	}
}
