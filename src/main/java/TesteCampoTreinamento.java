import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TesteCampoTreinamento {
	
	private WebDriver driver;
	private DSL dsl;
	
	
	@Before 
	public void inicializar () {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		dsl = new DSL(driver);  
  	
}
	
//	@After
	public void finalizar() {
		driver.quit();
}
	
	
	@Test
	public void testeTextField () {
		dsl.escrever("elementosForm:nome", "teste de escrita");
		Assert.assertEquals("teste de escrita", dsl.obterValorCampo("elementosForm:nome"));
		
	}
	
	@Test
	public void testTextFieldDuplo() {
		dsl.escrever("elementosForm:nome", "danilo");
		Assert.assertEquals("danilo", dsl.obterValorCampo("elementosForm:nome"));
		dsl.escrever("elementosForm:nome", "gomes");
		Assert.assertEquals("gomes", dsl.obterValorCampo("elementosForm:nome"));
	}
		
	
	@Test
	public void deveInteragirComTextArea() {
		dsl.escrever("elementosForm:sugestoes", "teste");
		Assert.assertEquals("teste", dsl.obterValorCampo("elementosForm:sugestoes"));
			
		
	}
	
	@Test
	public void deveInteragirComRadioButton() { 	
		dsl.clicarRadio("elementosForm:sexo:0");
		Assert.assertTrue(dsl.isRadioMarcado("elementosForm:sexo:0"));
				
	}
	
	@Test
	public void deveInteragirComCheckBox() { 
		dsl.clicarCheck("elementosForm:comidaFavorita:0");
		Assert.assertTrue(dsl.isCheckMarcado("elementosForm:comidaFavorita:0"));
	}
	
	@Test
	public void deveInteragirComCombo() { 
        dsl.selecionarCombo("elementosForm:escolaridade", "2o grau completo");
        Assert.assertEquals("2o grau completo", dsl.obterValorCampo("elementosForm:escolaridade"));
      
        
		
	}
	
	@Test
	public void deveVerificarValoresCombo() { 
        Assert.assertEquals(8, dsl.obterQuantidadeOpcoesCombo("elementosForm:escolaridade"));
        Assert.assertTrue(dsl.verificarOpcaoCombo("elementosForm:escolaridade", "Superior"));	    
	}
	
	@Test
	public void deveVerificarValoresComboMultiplos() { 
		dsl.selecionarCombo("elementosForm:esportes", "Natacao");
		dsl.selecionarCombo("elementosForm:esportes", "Corrida");
		dsl.selecionarCombo("elementosForm:esportes", "Karate");
       
        List<String> opcoesMarcadas = dsl.obterValorCombo("elementosForm:esportes");
        Assert.assertEquals(3, opcoesMarcadas.size());
        
        dsl.deselecionarCombo("elementosForm:esportes", "Corrida");
        opcoesMarcadas =dsl.obterValorCombo("elementosForm:esportes");
        Assert.assertEquals(2, opcoesMarcadas.size());
        Assert.assertTrue(opcoesMarcadas.containsAll(Arrays.asList("Natacao", "Karate")));
        
	}
	
	@Test
	public void deveInteragirComBotoes() { 
//		driver.findElement(By.id("buttonSimple")).click();
		dsl.clicarBotao("buttonSimple");
		Assert.assertEquals("Obrigado!", dsl.obterValueElement("buttonSimple"));		
	}
	
	@Test
//	@Ignore *lembrete de teste n�o verdadeiro*
	public void deveInteragirComLinks() { 
		dsl.clicarLink("Voltar");
//	    Assert.fail(); *lembrete de teste n�o verdadeiro
		Assert.assertEquals("Voltou!", dsl.obterTexto("resultado"));
		
		
	}
	
	@Test
	public void deveBuscartextosNaPagina() { 
//		System.out.println(driver.findElement(By.tagName("body")).getText()); *traz tudo que esta escrito na pagina*
//		Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("Campo de Treinamento"));
//		Assert.assertEquals("Campo de Treinamento", driver.findElement(By.tagName("h3")).getText());
		Assert.assertEquals("Campo de treinamente", dsl.obterTexto(By.tagName("h3")));
		Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", dsl.obterTexto(By.className("facilAchar")));
		
	
	}
}