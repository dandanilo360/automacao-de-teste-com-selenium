import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TesteFormulário {
	
	private WebDriver driver;
	private DSL dsl;
	private CampoTreinamentoPage page;
	
	
	@Before 
	public void inicializar () {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		dsl = new DSL(driver);  
  	    page = new CampoTreinamentoPage(driver);
}
	
	@After
	public void finalizar() {
	driver.quit();
}
	
	@Test
	public void deveRealizarCadastroComSucesso() { 
		page.setNome("Danilo");
		page.setSobrenome("Souza");
		page.setSexoMasculino();
		page.setComidaCarne();
		page.setEscolaridade("Superior");
		page.setEsporte("Corrida");
		page.cadastrar();
		
		Assert.assertTrue(page.obterResultadoCadastro().startsWith("Cadastrado!"));
		Assert.assertTrue(page.obterNomeCadastro().endsWith("Danilo"));
		Assert.assertEquals("Sobrenome: Souza", page.obterSobrenomeCadastro());
		Assert.assertEquals("Sexo: Masculino", page.obterSexoCadastro());
		Assert.assertEquals("Comida: Carne", page.obterComidaCadastro());
		Assert.assertEquals("Escolaridade: superior", page.obterEscolaridadeCadastro());
		Assert.assertEquals("Esportes: Corrida", page.obterEsportesCadastro());
		
	}
	
	@Test
	public void deveValidarNomeObridatorio() { 
		page.cadastrar();
		Assert.assertEquals("Nome eh obrigatorio", dsl.alertaObterTextoEAceita());	
	}
	
	@Test
	public void deveValidarSobreNomeObridatorio() { 
		page.setNome("Nome qualquer");
		page.cadastrar();
		Assert.assertEquals("Sobrenome eh obrigatorio", dsl.alertaObterTextoEAceita());
	}
	
	@Test
	public void deveValidarSexoObridatorio() { 		
		page.setNome("Nome qualquer");
		page.setSobrenome("sobrenome qualquer");
		page.cadastrar(); 
		Assert.assertEquals("Sexo eh obrigatorio",  dsl.alertaObterTextoEAceita());	
	}
	
	@Test
	public void deveValidarComidaObridatorio() { 
		page.setNome("Nome qualquer");
		page.setSobrenome("sobrenome qualquer");
		page.setSexoFeminino();
		page.setComidaCarne();
		page.setComidaVegetariano();
		page.cadastrar();
		Assert.assertEquals("Tem certeza que voce eh vegetariano?", dsl.alertaObterTextoEAceita());
	}
	
	@Test
	public void deveValidarEsporteObridatorio() { 
		page.setNome("Nome qualquer");
		page.setSobrenome("sobrenome qualquer");
		page.setSexoFeminino();
		page.setComidaCarne();
		page.setEsporte("Karate", "Voce faz esporte ou nao?");
		page.cadastrar();
		Assert.assertEquals("Voce faz esporte ou nao?", dsl.alertaObterTextoEAceita());
	}
}
