package TesteTudo;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TesteTestando {

    @Test
	public void TesteAreaNome() {   
    	System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.findElement(By.id("elementosForm:nome")).sendKeys("Testando escrita");
    	Assert.assertEquals("Testando escrita", driver.findElement(By.id("elementosForm:nome")).getAttribute("value"));
    	driver.quit();
    }
	
	 @Test
	 public void TesteAreaSorenome() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	    
		driver.findElement(By.id("elementosForm:sobrenome")).sendKeys("Testando sobrenome");
		Assert.assertEquals("Testando sobrenome", driver.findElement(By.id("elementosForm:sobrenome")).getAttribute("value"));
		driver.quit();
	}	  
	 
	 @Test
	 public void TesteAreaRadio() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		Assert.assertTrue(driver.findElement(By.id("elementosForm:sexo:0")).isSelected());
		driver.quit();
	 }
	 
	 @Test
	 public void TesteAreaCheckbox() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		driver.findElement(By.id("elementosForm:comidaFavorita:0")).click();
		Assert.assertTrue(driver.findElement(By.id("elementosForm:comidaFavorita:0")).isSelected());
		driver.quit();
	 }
	 
	 @Test
	 public void TesteComboEscolaridade() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	    WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
	    Select combo = new Select(element);
//	    combo.selectByValue("superior");
//	    combo.selectByIndex(4);
	    combo.selectByVisibleText("Superior");
	    
	    Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
	    driver.quit();
	    
	    }
	
	 /*
	 @Test
	 public void TesteComboListaEscolaridade() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().setSize(new Dimension(1200, 765));
		driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	    WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
	    Select combo = new Select(element);
	    List<WebElement> options = combo.getOptions();
	    Assert.assertEquals(8, options.size());
	    
	    boolean encontrou = false;
	    for (WebElement option: options) {
	    	if(option.getText().equals("Superior")) {
	    		encontrou = true;
	    		break;
	    	}
	    }
	    Assert.assertTrue(encontrou);
	    driver.quit();
	 }
    */
	 
	 /*
	 @Test
	 public void TesteComboEsportesdeMultiplasEscolhas() {
	  	System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
	  	WebDriver driver = new FirefoxDriver();
	  	driver.manage().window().setSize(new Dimension(1200, 765));
	  	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	    WebElement element = driver.findElement(By.id("elementosForm:esportes"));
	    Select combo = new Select(element);
	    combo.selectByVisibleText("Natacao");
	    combo.selectByVisibleText("Corrida");
	    combo.selectByVisibleText("O que eh esporte?");
	    
	    List<WebElement>allSelectedOptions = combo.getAllSelectedOptions();
	    Assert.assertEquals(3, allSelectedOptions.size());
	    
	    combo.deselectByVisibleText("O que eh esporte?");
	    allSelectedOptions = combo.getAllSelectedOptions();
	    Assert.assertEquals(2, allSelectedOptions.size());
	    driver.quit();
	 
	 }
     */
	 
	 @Test
     public void TesteTextArea() {
    	System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Teste Aerea Susget�es");
    	Assert.assertEquals("Teste Aerea Susget�es",driver.findElement(By.id("elementosForm:sugestoes")).getAttribute("value"));
    	driver.quit();
    }
	 
	@Test
 	public void TesteBotaoCliqueMe() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
		
    	WebElement botao = driver.findElement(By.id("buttonSimple"));
		botao.click();
		
		Assert.assertEquals("Obrigado!", botao.getAttribute("value"));
		driver.quit();
		/*
		driver.findElement(By.id("buttonSimple")).click();
		driver.findElement(By.id("buttonSimple")).getAttribute("value");
		driver.quit();
		*/
	}
	
	@Test
 	public void TesteLinks() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	//Falta verificar o retorno do link
    	driver.findElement(By.linkText("Voltar")).click();
    	Assert.assertEquals("Voltou!", driver.findElement(By.id("resultado")).getText());
    	driver.quit();
	}
    
	@Test
 	public void TesteDivs_E_Spans() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	
//	    Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("Campo de Treinamento"));
//    	Assert.assertEquals("Campo de Treinamento", driver.findElement(By.tagName("h3")).getText());
    	
    	Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", driver.findElement(By.className("facilAchar")).getText());
    	driver.quit();
	}
	
	@Test
 	public void TesteAlertSimples() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	
    	driver.findElement(By.id("alert")).click();
    	Alert alert = driver.switchTo().alert();
    	String texto = alert.getText();
    	Assert.assertEquals("Alert Simples", texto );
    	alert.accept();
    	
    	driver.findElement(By.id("elementosForm:nome")).sendKeys(texto);
    	driver.quit();
	}
	
	@Test
 	public void TesteConfirm() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.findElement(By.id("confirm")).click();
    	Alert alerta = driver.switchTo().alert();
    	Assert.assertEquals("Confirm Simples", alerta.getText());
    	alerta.dismiss();  	
    	Thread.sleep(1000);
    	Assert.assertEquals("Negado", alerta.getText());
    	alerta.accept();
    	
    	driver.findElement(By.id("confirm")).click();
    	alerta = driver.switchTo().alert();
    	Assert.assertEquals("Confirm Simples", alerta.getText());
    	alerta.accept();
    	Thread.sleep(1000);
    	Assert.assertEquals("Confirmado", alerta.getText());
    	alerta.accept();
    	
    	driver.quit();
	
	}
	
	@Test
 	public void TestePrompt() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	
    	driver.findElement(By.id("prompt")).click();
    	Alert alerta = driver.switchTo().alert();
    	Assert.assertEquals("Digite um numero", alerta.getText());
    	alerta.sendKeys("12");
    	alerta.accept();
//    	WebDriverWait wait = new WebDriverWait(driver, 30);
//    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Era 12")));
    	Assert.assertEquals("Era 12?", alerta.getText());
      	alerta.accept();
    	
      	Thread.sleep(1000); // Parada de 3sg na execu��o, para evitar o erro de lentid�o    
     	Assert.assertEquals(":D", alerta.getText());
    	alerta.accept();
    	
    	driver.quit();
    		
	}
	
	@Test
 	public void TesteCadastrar()  {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
	
    	driver.findElement(By.id("elementosForm:nome")).sendKeys("Danilo");
    	driver.findElement(By.id("elementosForm:sobrenome")).sendKeys("Souza");
    	driver.findElement(By.id("elementosForm:sexo:0")).click();	
    	driver.findElement(By.id("elementosForm:comidaFavorita:0")).click();
    	new Select(driver.findElement(By.id("elementosForm:escolaridade"))).selectByVisibleText("Superior"); // Para selecioner um elemento no combo
    	new Select(driver.findElement(By.id("elementosForm:esportes"))).selectByVisibleText("Corrida");
    	driver.findElement(By.id("elementosForm:cadastrar")).click();
    	
    	Assert.assertTrue(driver.findElement(By.id("resultado")).getText().startsWith("Cadastrado!")); // Aqui verifica o inicio 
    	Assert.assertTrue(driver.findElement(By.id("descNome")).getText().endsWith("Danilo")); // Aqui verifica o final 
    	Assert.assertEquals("Sobrenome: Souza", driver.findElement(By.id("descSobrenome")).getText());
    	Assert.assertEquals("Sexo: Masculino", driver.findElement(By.id("descSexo")).getText());
    	Assert.assertEquals("Comida: Carne", driver.findElement(By.id("descComida")).getText());
    	Assert.assertEquals("Escolaridade: superior", driver.findElement(By.id("descEscolaridade")).getText());
    	Assert.assertEquals("Esportes: Corrida", driver.findElement(By.id("descEsportes")).getText());
    	
    	driver.quit(); 	
	}
	
	@Test
 	public void TesteBotaoFrame() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.switchTo().frame("frame1");
    	driver.findElement(By.id("frameButton")).click();
    	Alert alert = driver.switchTo().alert(); 
    	String msg = alert.getText(); // confirmar a mensagem
    	Assert.assertEquals("Frame OK!", msg); // confirm alert ok
    	alert.accept(); // fecha o alert
    	
    	driver.switchTo().defaultContent();
    	
    	Thread.sleep(1000);
    	driver.findElement(By.id("elementosForm:nome")).sendKeys("Frame OK");
    	driver.quit();
	
	}
	
	@Test
 	public void TesteJanelasEPopup() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.findElement(By.id("buttonPopUpEasy")).click();
    	driver.switchTo().window("Popup");  // trocar o foco para a janela PopUp
    	driver.findElement(By.tagName("textarea")).sendKeys("Deu Certo?");
    	driver.close();
    	
    	
//    	driver.switchTo().window(" ");
//    	driver.findElement(By.tagName("textarea")).sendKeys("Espero que sim!");
    	driver.quit();
	}
	
	@Test
 	public void TesteJanelasEPopupSemTitle() {
		System.setProperty("webdriver.gecko.driver","/Users/danda/Downloads/driver/geckodriver.exe" );
    	WebDriver driver = new FirefoxDriver();
    	driver.manage().window().setSize(new Dimension(1200, 765));
    	driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    	
    	driver.findElement(By.id("buttonPopUpHard")).click();
    	System.out.println(driver.getWindowHandle()); // pegar ID de uma janela sem titulo
    	System.out.println(driver.getWindowHandles()); // pega todos ID�s das janelas abertas
    	driver.switchTo().window((String)driver.getWindowHandles().toArray()[1]);  // mudar o foco da janela para excrever 
    	driver.findElement(By.tagName("textarea")).sendKeys("Deu Certo?");
    	
    	driver.switchTo().window((String)driver.getWindowHandles().toArray()[0]); // volta o foco para janela central
    	driver.findElement(By.tagName("textarea")).sendKeys("e ai, deu certo?");
    	driver.quit();
	}
}
